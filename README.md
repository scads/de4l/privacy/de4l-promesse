# de4l-promesse

Privacy algorithm to disguise places of interest (POI) in a route of geographical points, 
that can be revealed by longer stays in a region. Implementation according to the `Promesse` algorithm
by Vincent Primault:

Vincent Primault. Practically Preserving and Evaluating Location Privacy. Cryptography and Security [cs.CR]. 
Université de Lyon; INSA Lyon, 2018. English. NNT : 2018LYSEI017. tel-01806701v1

Available at: https://tel.archives-ouvertes.fr/tel-01806701v1

Install this package in your repository via pip:
```bash
pip install git+https://git@git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-promesse.git
```