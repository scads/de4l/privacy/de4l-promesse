"""This module provides a privacy algorithm to disguise places of interest (POI) in a route of geographical points,
that can be revealed by longer stays in a region. The implementation is done according to the `Promesse` algorithm
by Vincent Primault:

Vincent Primault. Practically Preserving and Evaluating Location Privacy. Cryptography and Security [cs.CR].
Université de Lyon; INSA Lyon, 2018. English. NNT : 2018LYSEI017. tel-01806701v1

Available at: https://tel.archives-ouvertes.fr/tel-01806701v1
"""

from de4l_geodata.geodata.route import Route
from de4l_geodata.geodata.point_t import get_interpolated_point
from de4l_geodata.geodata.point import get_distance


def speed_smoothing(route, alpha):
    """
    Protects places of interest (POI) in a route of geographical points by sampling points along the shape of the
    original route, where successive points have a predefined distance of alpha from each other. This avoids point
    clusters, which indicate longer stays of a person and can therefore identify POIs.

    The algorithm is implemented according to the algorithm 'Promesse' from Vincent Primault, which has been introduced
    in the following paper (algorithm 2 on page 61):

    Vincent Primault (2018). Practically Preserving and Evaluating Location Privacy. Cryptography and Security [cs.CR].
    Université de Lyon; INSA Lyon, 2018. English. NNT : 2018LYSEI017. tel-01806701v1

    It is available at: https://tel.archives-ouvertes.fr/tel-01806701v1

    How it works:

    Samples points along the shape that is formed when connecting successive points of the original route, where each
    two successive points have a distance of at least alpha. The first and last point of the route are removed to avoid
    leaking potentially private information. In a second step the timestamps of the sampled points are set to be
    equally distributed between the original start and end timestamps.

    Parameters
    ----------
    route: Route
        A route that contains points of type geodata.point_t.PointT.
    alpha: float
        Minimum distance in meters between successive points. A larger alpha provides better privacy but leads to a
        higher loss of information.

    Returns
    -------
    sampled_route: Route
        A route with speed smoothed points or an empty route if the original route can not be protected with the
        indicated parameters.
    """

    if alpha <= 0:
        raise ValueError("Alpha needs to be greater than zero.")

    sampled_route = Route()
    previous_point = None

    for current_point in route:
        if previous_point is None:
            sampled_route.append(current_point)
            previous_point = current_point
        else:
            distance = get_distance(previous_point, current_point)
            while distance >= alpha:
                # sample new points by interpolating between successive points
                psi = alpha / distance
                interpolated_point = get_interpolated_point(previous_point, current_point, psi)
                interpolated_point.timestamp = current_point.timestamp
                sampled_route.append(interpolated_point)
                previous_point = interpolated_point
                distance = get_distance(previous_point, current_point)

    # remove first and last point from route
    if len(sampled_route) > 2:
        sampled_route.delete_point_at_(0)
        sampled_route.delete_point_at_(len(sampled_route) - 1)

    if len(sampled_route) > 2:
        first_timestamp = sampled_route[0].timestamp
        last_timestamp = sampled_route[len(sampled_route) - 1].timestamp
        delta = (last_timestamp - first_timestamp) / (len(sampled_route) - 1)

        current_timestamp = first_timestamp
        for current_point in sampled_route:
            current_point.timestamp = current_timestamp
            current_timestamp += delta
    else:
        # if the sampled route does not contain enough points, return an empty route
        sampled_route = Route()

    return sampled_route
