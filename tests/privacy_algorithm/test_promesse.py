"""Test of 'Promesse' algorithm.
"""

import unittest
from math import radians

from de4l_geodata.geodata.point import get_distance
from de4l_geodata.geodata.route import Route
from de4l_geodata.geodata.point_t import PointT
from pandas import Timestamp

from de4l_promesse.privacy_algorithm.promesse import speed_smoothing


class TestServicePromesse(unittest.TestCase):
    """Test of 'Promesse' algorithm.
    """

    def test_speed_smoothing(self):
        """Test of the speed smoothing functionality of 'Promesse' algorithm.
        """
        points = {'bus_main_station': PointT([radians(12.38196), radians(51.34374)], Timestamp('2021-10-05 12:00:00')),
                  'intercity_hotel': PointT([radians(12.37607), radians(51.34473)], Timestamp('2021-10-05 12:01:00')),
                  'sab': PointT([radians(12.37569), radians(51.34544)], Timestamp('2021-10-05 12:01:01')),
                  'packhof_street': PointT([radians(12.37566), radians(51.34491)], Timestamp('2021-10-05 12:01:05')),
                  'district_office': PointT([radians(12.37480), radians(51.34582)], Timestamp('2021-10-05 12:01:20')),
                  'santander_bank': PointT([radians(12.37462), radians(51.34454)], Timestamp('2021-10-05 12:01:40')),
                  'troendlinring': PointT([radians(12.37204), radians(51.34480)], Timestamp('2021-10-05 12:02:50')),
                  'natural_history_museum': PointT([radians(12.37085), radians(51.34498)],
                                                   Timestamp('2021-10-05 12:03:00')),
                  'apartment_central_hotel': PointT([radians(12.37135), radians(51.34542)],
                                                    Timestamp('2021-10-05 12:05:00')),
                  'pfaffendorfer_bruecke': PointT([radians(12.37127), radians(51.34804)],
                                                  Timestamp('2021-10-05 12:05:02')),
                  'zoo': PointT([radians(12.37151), radians(51.34851)], Timestamp('2021-10-05 12:05:10'))}

        route_empty = Route()
        route_one_element = Route([points['bus_main_station']])
        route_two_elements = Route([points['bus_main_station'], points['zoo']])
        route_three_elements = Route([points['bus_main_station'], points['packhof_street'], points['zoo']])
        route_lpz = Route([points['bus_main_station'], points['intercity_hotel'], points['sab'],
                           points['packhof_street'], points['district_office'], points['santander_bank'],
                           points['troendlinring'], points['natural_history_museum'], points['apartment_central_hotel'],
                           points['pfaffendorfer_bruecke'], points['zoo']])

        for alpha in [-100, -50, 0, 1, 50.5, 100, 150, 200, 300.75, 500, 1000]:
            if alpha <= 0:
                self.assertRaises(ValueError)
            else:
                # check routes that are too short to be protected
                for route in [route_empty, route_one_element]:
                    self.assertEqual(Route(), speed_smoothing(route, alpha))

                for route in [route_two_elements, route_three_elements, route_lpz]:
                    smoothed_route = speed_smoothing(route, alpha)
                    for idx in range(1, len(smoothed_route)):
                        # check that the timedelta is equal between successive points
                        timedelta_first_two_points = smoothed_route[1].timestamp - smoothed_route[0].timestamp
                        timedelta_successive_points = smoothed_route[idx].timestamp - smoothed_route[idx-1].timestamp
                        self.assertEqual(timedelta_first_two_points, timedelta_successive_points)
                        # check that successive points have the requested spatial distance between points
                        self.assertAlmostEqual(alpha, get_distance(smoothed_route[idx-1], smoothed_route[idx]),
                                               places=3)


if __name__ == "__main__":
    unittest.main()
